using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int ZombiesAmount = 5;
    public TextMeshProUGUI textZombies;

    void Start()
    {
        textZombies.text = ZombiesAmount.ToString();
        Cursor.lockState = CursorLockMode.None;
    }
    // Update is called once per frame
    void Update()
    {
        textZombies.text = ZombiesAmount.ToString();
        if (ZombiesAmount == 0)
        {
            SceneManager.LoadScene("ResultScene");
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

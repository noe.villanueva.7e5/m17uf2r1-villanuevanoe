using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public int HP = 100;
    public Animator animator;

    public void TakeDamage(int damageAmount)
    {
        Debug.Log("Au");
        HP -= damageAmount;
        if (HP <= 0)
        {
            animator.SetTrigger("die");
            GetComponent<Collider>().enabled = false;
            StartCoroutine(DestroyZombie());
            GameManager.ZombiesAmount -= 1;
        }
        else
        {
            animator.SetTrigger("damage");
        }    
    }
    
    IEnumerator DestroyZombie()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}

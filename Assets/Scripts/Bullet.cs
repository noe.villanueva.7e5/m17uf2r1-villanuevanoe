using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int damageAmount = 20;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyBullets());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Zombie")
        {
            other.GetComponent<Zombie>().TakeDamage(damageAmount);
        }

        if(other.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator DestroyBullets()
    {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
}

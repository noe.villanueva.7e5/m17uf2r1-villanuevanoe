using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerMovement: MonoBehaviour
{
    // Configuraci� del moviment
    public float speed;
    public float walkSpeed = 5.0f;
    public float runSpeed = 10.0f;
    public float jumpForce = 0.5f;
    public float gravity;
    private float moveY;

    // Estat del jugador
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private Transform _mainCamera;
    public Animator playerAnimator;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private bool dance;
    private Vector3 moveDir;
    public CinemachineVirtualCamera virtualCamera;
    private States state;

    void Start()
    {
        // Obtenir el component CharacterController
        controller = GetComponent<CharacterController>();
        _mainCamera = Camera.main.transform;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        SetGravity();
        Movement();
        Jump();
        Cheer();
        PickUp();
        Crouch();

        /*switch (state)
        {
            case States.Moving:
                SetGravity();
                Movement();
                Jump();
                break;
            case States.Dancing:
                Cheer();
                break;
            case States.PickingObject:
                PickUp();
                break;
            case States.Crouching:
                Crouch();
                break;
            case States.Aiming:
                
                break;
            default: break;
        }*/
    }
    void Movement()
    {
        States state = States.Moving;
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");
        Vector3 playerInput = new Vector3(hAxis, 0f, vAxis);

        if (playerInput.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(playerInput.x, playerInput.z) * Mathf.Rad2Deg + _mainCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Run();
            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

        }
        else
        {
            speed = 0f;
            playerAnimator.SetFloat("speed", 0f);
        }
        controller.Move((moveDir.normalized * speed + Vector3.up * moveY) * Time.deltaTime);


    }
    void Run()
    {
        States state = States.Moving;
        // Detectar si s'est� fent c�rrer al clicar el shift
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = runSpeed;
            playerAnimator.SetFloat("speed", runSpeed / runSpeed);
        }
        else
        {
            speed = walkSpeed;
            playerAnimator.SetFloat("speed", walkSpeed / runSpeed);
        }
    }
    void Jump()
    {
        States state = States.Moving;
        // Aplicar la gravetat
        if (controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            moveY = jumpForce;
            playerAnimator.SetTrigger("isJumping");
            
        }
    }
    void SetGravity()
    {
        States state = States.Moving;
        if (!controller.isGrounded)
        {
            moveY += gravity * Time.deltaTime;
        }
    }

    void Cheer()
    {
        States state = States.Dancing;
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            virtualCamera.Priority += 1;
            StartCoroutine(WaitToCheer());
            StartCoroutine(BackToMainCamera());
        }
    }

    void PickUp()
    {
        States state = States.PickingObject;
        if (Input.GetKeyDown(KeyCode.E))
        {
            playerAnimator.SetTrigger("isPicking");
        }
    }

    void Crouch()
    {
        States state = States.Crouching;
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            playerAnimator.SetTrigger("isCrouching");
        }
    }

    IEnumerator WaitToCheer()
    {
        yield return new WaitForSeconds(1.5f);
        playerAnimator.SetTrigger("isCheering");
    }

    IEnumerator BackToMainCamera()
    {
        yield return new WaitForSeconds(3f);
        virtualCamera.Priority -= 1;
    }
    public enum States
    {
        Moving,
        Aiming,
        Dancing,
        PickingObject,
        Crouching
    }
}
# M17UF2R1 - VillanuevaNoe

# Game Controls

- A,W,S,D to control the player

- Left Shift while you are moving, you can run

- Left Control to crouch

- Key Number 3 to celebrate

- E to pick an item

- Mouse Right Click you can aim

- Mouse Left Click while you are aiming, you can shoot

- Moving the mouse, you move the camera

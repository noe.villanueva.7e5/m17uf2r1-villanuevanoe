using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private GameObject bulletPoint;
    [SerializeField]
    private float bulletSpeed = 600;
    [SerializeField]
    private float fireRate = 0.55f;
    float nextFire;
    public Animator animator;
    public CinemachineVirtualCamera virtualCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            //PlayerMovement.States state = PlayerMovement.States.Aiming;
            animator.SetBool("isAiming", true);
            virtualCamera.Priority += 1;

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                animator.SetTrigger("shoot");
                Shoot();
            }

        }else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            animator.SetBool("isAiming", false);
            virtualCamera.Priority = 9;
        }
    }

    void Shoot()
    {
        if(Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Debug.Log("Shoot!");
            GameObject bullet = Instantiate(bulletPrefab, bulletPoint.transform.position, transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed);
        }
    }
}
